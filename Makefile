build:
	mkdir -p .src/Engine/ && cp -R src/Engine/ .src/Engine/
	find .src/Engine -type f -name "*.php" -exec mv {} {}.dist \;
	box build -v
	chmod +x marvin.phar
	rm -rf .src/

clean:
	rm -f marvin.phar

install:
	composer install
	box build -v

start:
	bin/marvin server:start

<?php declare(strict_types=1);

namespace Tests\Engine\Batch;

use PHPUnit\Framework\TestCase;
use Marvin\Executor\EngineBase;
use Marvin\Engine\Batch\ModelTraining;

class ModelTrainingTest extends TestCase
{
    public function testInitial()
    {
        $engine = new ModelTraining();

        $this->assertInstanceOf(ModelTraining::class, $engine);
        $this->assertInstanceOf(EngineBase::class, $engine);
    }
}

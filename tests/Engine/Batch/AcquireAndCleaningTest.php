<?php declare(strict_types=1);

namespace Tests\Engine\Batch;

use Marvin\Engine\Batch\AcquireAndCleaning;
use PHPUnit\Framework\TestCase;
use Marvin\Executor\EngineBase;

class AcquireAndCleaningTest extends TestCase
{
    public function testInitial()
    {
        $engine = new AcquireAndCleaning();

        $this->assertInstanceOf(AcquireAndCleaning::class, $engine);
        $this->assertInstanceOf(EngineBase::class, $engine);
    }
}

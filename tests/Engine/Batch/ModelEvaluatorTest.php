<?php declare(strict_types=1);

namespace Tests\Engine\Batch;

use PHPUnit\Framework\TestCase;
use Marvin\Executor\EngineBase;
use Marvin\Engine\Batch\ModelEvaluator;

class ModelEvaluatorTest extends TestCase
{
    public function testInitial()
    {
        $engine = new ModelEvaluator();

        $this->assertInstanceOf(ModelEvaluator::class, $engine);
        $this->assertInstanceOf(EngineBase::class, $engine);
    }
}

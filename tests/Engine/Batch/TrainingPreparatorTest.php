<?php declare(strict_types=1);

namespace Tests\Engine\Batch;

use PHPUnit\Framework\TestCase;
use Marvin\Executor\EngineBase;
use Marvin\Engine\Batch\TrainingPreparator;

class TrainingPreparatorTest extends TestCase
{
    public function testInitial()
    {
        $engine = new TrainingPreparator();

        $this->assertInstanceOf(TrainingPreparator::class, $engine);
        $this->assertInstanceOf(EngineBase::class, $engine);
    }
}

<?php declare(strict_types=1);

namespace Tests\Engine\Online;

use PHPUnit\Framework\TestCase;
use Marvin\Executor\EngineBase;
use Marvin\Engine\Online\ModelPrediction;

class ModelPredictionTest extends TestCase
{
    public function testInitial()
    {
        $engine = new ModelPrediction();

        $this->assertInstanceOf(ModelPrediction::class, $engine);
        $this->assertInstanceOf(EngineBase::class, $engine);
    }
}

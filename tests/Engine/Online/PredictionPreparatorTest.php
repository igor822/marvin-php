<?php declare(strict_types=1);

namespace Tests\Engine\Online;

use PHPUnit\Framework\TestCase;
use Marvin\Executor\EngineBase;
use Marvin\Engine\Online\PredictionPreparator;

class PredictionPreparatorTest extends TestCase
{
    public function testInitial()
    {
        $engine = new PredictionPreparator();

        $this->assertInstanceOf(PredictionPreparator::class, $engine);
        $this->assertInstanceOf(EngineBase::class, $engine);
    }
}

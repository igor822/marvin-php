<?php declare(strict_types=1);

namespace Tests\Artifact\Component;

use Marvin\Artifact\Component\UUID;
use PHPUnit\Framework\TestCase;

class UUIDTest extends TestCase
{
    public function testInitial()
    {
        $uuid = UUID::v4();

        $this->assertInternalType('string', $uuid);
    }
}

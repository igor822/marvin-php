<?php declare(strict_types=1);

namespace Tests\Artifact;

use Marvin\Artifact\FileSystemArtifact;
use PHPUnit\Framework\TestCase;
use Marvin\Artifact\Component\UUID;

class FileSystemArtifactTest extends TestCase
{
    public function testInitial()
    {
        $artifact = new FileSystemArtifact('');

        $this->assertInstanceOf(FileSystemArtifact::class, $artifact);
    }

    public function testStoreFile()
    {
        $artifact = new FileSystemArtifact('');
        $filename = $artifact->store(['teste']);
        $this->assertTrue(file_exists(FileSystemArtifact::TMP_PATH . $filename));
    }

    public function testCheckObjectStored()
    {
        $object = ['teste'];
        $artifact = new FileSystemArtifact('test');
        $filename = $artifact->store($object);

        $contents = file_get_contents(FileSystemArtifact::TMP_PATH . 'test/' . $filename);
        $this->assertEquals(serialize($object), $contents);
    }

    public function testRetrieveContents()
    {
        $artifactName = 'test';
        $object = ['teste', 'testing1', 'testing2'];
        $artifact = new FileSystemArtifact($artifactName);
        $filename = $artifact->store($object);

        $retrievedContent = $artifact->retrieve();
        $this->assertEquals($object, $retrievedContent);
    }

    public function testRetrieveEmptyContent()
    {
        $artifactName = 'teste';
        $artifact = new FileSystemArtifact($artifactName);
        $retrievedContent = $artifact->retrieve();
        $this->assertEmpty($retrievedContent);
    }

    /**
     * @depends testCheckObjectStored
     */
    public function testCheckIfArtifactExists()
    {
        $artifactName = 'test';
        $artifact = new FileSystemArtifact($artifactName);
        $this->assertTrue($artifact->hasArtifact());
    }
}

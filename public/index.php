<?php
if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

if (getenv('ROOT_PATH') && !defined('ROOT_PATH')) {
    define('ROOT_PATH', base64_decode(getenv('ROOT_PATH')));
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

$app = require ROOT_PATH . '/app/bootstrap.php';

// Run app
$app->run();

<?php

// Instantiate the app
$settings = require ROOT_PATH . '/app/settings.php';
$app = new \Slim\App($settings);

// Set up dependencies
require ROOT_PATH . '/app/dependencies.php';

// Register middleware
require ROOT_PATH . '/app/middleware.php';

// Register routes
require ROOT_PATH . '/app/routes.php';

return $app;

<?php

namespace Marvin\Artifact;

interface ArtifactInterface
{
    public function store($object): string;

    public function hasArtifact(): bool;

    public function retrieve();
}

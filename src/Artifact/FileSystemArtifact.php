<?php

namespace Marvin\Artifact;

use Marvin\Artifact\Component\UUID;

class FileSystemArtifact extends AbstractArtifactSaver
{
    public function store($object): string
    {
        $this->clear();
        $filename = UUID::v4();
        $path = self::TMP_PATH . $this->name . '/' . $filename;
        $file = fopen($path, 'w');
        fwrite($file, serialize($object));
        fclose($file);

        return $filename;
    }

    public function hasArtifact(): bool
    {
        return count(glob(self::TMP_PATH . $this->name . '/')) ? true : false;
    }

    public function retrieve()
    {
        if (!$this->hasArtifact()) {
            return null;
        }
        $contents = '';
        $files = glob(self::TMP_PATH . $this->name . '/*');
        foreach ($files as $file) {
            $contents = file_get_contents($file);
        }

        return unserialize($contents);
    }

    protected function clear(): void
    {
        $files = glob(self::TMP_PATH . $this->name . '/*');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }
    }

    protected function prepare(): void
    {
        if (!is_dir(self::TMP_PATH . $this->name)) {
            mkdir(self::TMP_PATH . $this->name, 0777);
        }
    }
}

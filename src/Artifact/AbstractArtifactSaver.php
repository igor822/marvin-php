<?php

namespace Marvin\Artifact;

abstract class AbstractArtifactSaver implements ArtifactInterface
{
    const TMP_PATH = '/tmp/artifacts/';

    protected $name;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->prepare();
    }

    public function getName(): string
    {
        return $this->name;
    }

    abstract protected function clear(): void;

    abstract protected function prepare(): void;
}

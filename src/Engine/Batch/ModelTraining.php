<?php

namespace Marvin\Engine\Batch;

use Marvin\Executor\EngineBase;

class ModelTraining extends EngineBase
{
    public function execute(array $params = [], $artifact = null): array
    {
        echo self::class . PHP_EOL;

        return ['ModelTraining content'];
    }
}

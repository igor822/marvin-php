<?php

namespace Marvin\Engine\Batch;

use Marvin\Executor\EngineBase;

class TrainingPreparator extends EngineBase
{
    public function execute(array $params = [], $artifact = null): array
    {
        echo self::class . PHP_EOL;

        return ['TrainingPreparator content'];
    }
}

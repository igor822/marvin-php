<?php

namespace Marvin\Engine\Online;

use Marvin\Executor\EngineBase;

class ModelPrediction extends EngineBase
{
    public function execute(array $inputMessage = [], $artifact = null): array
    {
        echo self::class . PHP_EOL;

        return ['ModelPrediction content'];
    }
}

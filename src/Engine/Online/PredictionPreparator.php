<?php

namespace Marvin\Engine\Online;

use Marvin\Executor\EngineBase;

class PredictionPreparator extends EngineBase
{
    public function execute(array $inputMessage = [], $artifact = null): array
    {
        echo self::class . PHP_EOL;

        return ['PredictionPreparator content'];
    }
}

<?php

namespace Marvin\Command\Engine;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class EngineGenerateCommand extends Command
{
    protected function configure()
    {
        $this->setName('engine:generate')
             ->addOption('name', 'na', InputOption::VALUE_REQUIRED, 'Project name', 'Project name')
             ->addOption('description', 'd', InputOption::VALUE_OPTIONAL, 'Short Description', 'Marvin Project')
             ->addOption('path', 'p', InputOption::VALUE_OPTIONAL, 'Path of project', getcwd());
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $project = [
            'name' => $input->getOption('name'),
            'description' => $input->getOption('description')
        ];

        $fileSystem = new Filesystem();
        $rootProject = $input->getOption('path');
        if (!file_exists($rootProject)) {
            $fileSystem->mkdir($rootProject);
        }

        $folders = [
            $rootProject . '/bin/',
            $rootProject . '/src/',
            $rootProject . '/src/Engine/',
            $rootProject . '/src/Engine/Batch/',
            $rootProject . '/src/Engine/Online/',
            $rootProject . '/public',
        ];

        foreach ($folders as $internalPath) {
            $fileSystem->mkdir($internalPath);
        }

        $filesToCopy = [
            ROOT_PATH . '/composer.json.dist' => $rootProject . '/composer.json',
            ROOT_PATH . '/engine.params' => $rootProject . '/engine.params',
            ROOT_PATH . '/public/index.php' => $rootProject . '/public/index.php',
            ROOT_PATH . '/.src/Engine/Batch/AcquireAndCleaning.php.dist' => $rootProject . '/src/Engine/Batch/AcquireAndCleaning.php',
            ROOT_PATH . '/.src/Engine/Batch/ModelEvaluator.php.dist' => $rootProject . '/src/Engine/Batch/ModelEvaluator.php',
            ROOT_PATH . '/.src/Engine/Batch/ModelTraining.php.dist' => $rootProject . '/src/Engine/Batch/ModelTraining.php',
            ROOT_PATH . '/.src/Engine/Batch/TrainingPreparator.php.dist' => $rootProject . '/src/Engine/Batch/TrainingPreparator.php',
            ROOT_PATH . '/.src/Engine/Online/ModelPrediction.php.dist' => $rootProject . '/src/Engine/Online/ModelPrediction.php',
            ROOT_PATH . '/.src/Engine/Online/PredictionPreparator.php.dist' => $rootProject . '/src/Engine/Online/PredictionPreparator.php',
        ];

        foreach ($filesToCopy as $from => $to) {
            $fileSystem->copy($from, $to);
        }
    }
}

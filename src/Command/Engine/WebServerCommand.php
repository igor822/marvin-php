<?php

namespace Marvin\Command\Engine;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WebServerCommand extends Command
{
    protected function configure()
    {
        $this->setName('server:start');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        echo 'ROOT_PATH=' . base64_encode(ROOT_PATH) . ' php -S localhost:8000 -t ' . getcwd() . '/public';
        echo system('ROOT_PATH=' . base64_encode(ROOT_PATH) . ' php -S localhost:8000 -t ' . getcwd() . '/public');
    }
}

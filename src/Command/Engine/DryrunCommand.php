<?php

namespace Marvin\Command\Engine;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Marvin\Engine\Batch\AcquireAndCleaning;
use Marvin\Engine\Batch\TrainingPreparator;
use Marvin\Engine\Batch\ModelTraining;
use Marvin\Engine\Batch\ModelEvaluator;
use Marvin\Engine\Online\PredictionPreparator;
use Marvin\Engine\Online\ModelPrediction;

class DryrunCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('engine:dryrun')
            ->addOption('action', 'a', InputOption::VALUE_REQUIRED, 'Marvin engine action name', 'all');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $option = $input->getOption('action');
        $engine = null;
        switch (true) {
            case in_array($option, ['acquisitor', 'all']):
                $engine = new AcquireAndCleaning();
                $engine->handle();
            case in_array($option, ['tpreparator', 'all']):
                $engine = new TrainingPreparator();
                $engine->handle();
            case in_array($option, ['trainer', 'all']):
                $engine = new ModelTraining();
                $engine->handle();
            case in_array($option, ['evaluator', 'all']):
                $engine = new ModelEvaluator();
                $engine->handle();
            case in_array($option, ['ppreparator', 'all']):
                $engine = new PredictionPreparator();
                $engine->handle();
            case in_array($option, ['predictor', 'all']):
                $engine = new ModelPrediction();
                $engine->handle();
            case $option == 'feedback':
                break;
        }
    }
}

<?php

namespace Marvin\Executor;

use Marvin\Artifact\FileSystemArtifact;

abstract class EngineBase
{
    private $engines = [
        'AcquireAndCleaning',
        'TrainingPreparator',
        'ModelTraining',
        'ModelEvaluator',
        'PredictionPreparator',
        'ModelPrediction'
    ];

    abstract public function execute(array $params = []): array;

    public function handle(): void
    {
        $previousArtifact = $this->getPreviousArtifact();
        $artifact = new FileSystemArtifact($this->getArtifactName());
        if (isset($previousArtifact)) {
            $artifact = $previousArtifact;
        }

        $response = $this->execute($this->loadParams(), $artifact->retrieve());
        $artifact->store($response);
    }

    public function getArtifactName(): string
    {
        return substr(strrchr(get_class($this), '\\'), 1);
    }

    public function loadParams(): array
    {
        if (!file_exists('engine.params')) {
            return [];
        }

        $params = file_get_contents('engine.params');
        $params = json_decode($params, true);
        if (!$params) {
            throw new \Exception(sprintf('JSON error: %s', json_last_error_msg()));
        }
        return $params;
    }

    public function getPreviousArtifact(): ?FileSystemArtifact
    {
        $currentArtifact = $this->getArtifactName();
        $enginePosition = array_search($currentArtifact, $this->engines);
        if ($enginePosition == 0) {
            return null;
       }

        return (new FileSystemArtifact($this->engines[$enginePosition - 1]));
    }
}

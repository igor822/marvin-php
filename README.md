Marvin PHP
===================

This project has the ability of simulate the Marvin architecture for PHP and it's inspired by the project [Marvin Toolbox](https://github.com/marvin-ai/marvin-python-toolbox)


# Commands

```sh
php bin/marvin.php engine:dryrun
```

## Requirements

* PHP 7+

## Install 

For development purpose you must use the box2 for packing this library

### Install box

```sh
curl -LSs https://box-project.github.io/box2/installer.php | php
```

You must edit the `php.ini` file by setting `phar.readonly = Off` for box2 to be allowed to generate the `phar` package

### Build package

```sh
make build
```
